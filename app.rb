# -*- coding: utf-8 -*-
module Kura
  require 'sinatra/base'
  require 'sinatra/jsonp'
  require 'active_record'

  class App < Sinatra::Base
    config = YAML.load_file './database.yml'
    ActiveRecord::Base.establish_connection(config["db"]["development"])
  end

  require_relative 'models/init'
  require_relative 'routes/init'
end
