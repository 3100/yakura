# encoding: UTF-8

#root = File.dirname(__FILE__)
#require File.join(root, 'app')
require 'rubygems'
require 'sinatra'
require File.expand_path './app', __FILE__

set :envitonment, ENV['RACK_ENV'].to_sym
#disable :run

run Kura::App
