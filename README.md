# yakura (yet another kura)

MariaDBを画像保存先として使う単純なWebアプリのデモです。

* Ruby1.9.3
* Sinatra

Passenger3を用いて、Apacheと連携できることを確認しています。(Passenger4ではエラーが出る)

## 概要

* 画像はstaticなファイルとしてではなく、DBにもたせています。
 * 各画像はGETされる際にDBから動的に生成。(将来的なアクセス制御実装を考慮)
