class Photos < ActiveRecord::Migration
  def up
    create_table :photos, :primary_key => :photo_id do |t|
      t.integer :customer_id, :null => false, :default => 0
      t.string :comment
      t.column :thumb, 'longblob'
      t.column :raw, 'longblob', :null => false
      t.timestamps
    end
  end

  def down
    drop_table :photos
  end
end
