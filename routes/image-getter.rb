# -*- coding: utf-8 -*-
module Kura
  require 'sinatra/base'
  require 'slim'

  class App < Sinatra::Base
    get '/' do
      @photos = Model::Photo.all
      slim :index
    end

    get '/image/:id' do |id|
      candidate = Model::Photo.select {|p| p.photo_id == id.to_i}
      return 'error' unless candidate.any?
      content_type 'image/jpeg'
      candidate.first.raw
    end

    get '/upload' do
      slim :upload
    end

    post '/upload' do
      p params[:photo]
      photo = Model::Photo.new
      photo.raw = params[:photo][:tempfile].read
      photo.save
      redirect '/'
    end
  end
end

